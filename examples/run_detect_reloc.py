#!/usr/bin/env python

# External modules
import sys
from obspy.core import UTCDateTime

# Internal modules
from Detect import Detect

def main(argv):

    # Detect object
    config_file = argv[1]
    detect = Detect(config_file)
    
    # Get templates
    detect.get_templates(config_file,request_data=True,overwrite=False)

    # Iterate over days (1 run directory/day)
    for d in detect:
        # Get continuous waveforms
        d.get_continuous_waveforms(overwrite=False)
        # Prepare run directory
        d.prepare_run(cleanup=True)
        # Run detections/relocs
        d.run(detect=True,compute_delays=True,reloc=True,remove_continuous=True,cleanup=False)
        #d.run(detect=True,compute_delays=True,reloc=True,remove_continuous=False,cleanup=False)

    # All done
    return

if __name__=='__main__':
    main(sys.argv)

