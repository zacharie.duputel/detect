# Detect

A basic python wrapper for the DETX Template Matching code (cf., Lengline et al., 2016; Duputel et al., 2019).

## Dependancies
* python3
* python modules :
    * numpy, 
    * obspy, 
    * configparser (Configuration file parser), 
    * paramiko (SSH2 protocol library), 
    * scp (scp module for paramiko)

## Some instructions
To use detect, the detect directory must be placed in a path pointed by the PYTHONPATH environment variable. 

To check that it's correctly pointed by your PYTHON path, in your python shell:
```
python -c "import detect"
```

An example of script using detect is provided in `examples/` along with a configuration file (cf., `examples/config_vsup.rc`) 
and event bulletins (in `examples/billetin`).


