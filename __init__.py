'''
Init file for Detect

Written by Z. Duputel, July 2016
'''

# Base class
from .Detect import Detect
from .TemplateRequest import TemplateRequest
from .GetWaveforms import get_OVPF_mseed_data
