
import os,sys,shutil,tempfile
import configparser
from obspy.core import UTCDateTime, read
from paramiko import SSHClient, AutoAddPolicy
from scp import SCPClient

def get_st(filename) :
    st = read(filename)
    st.detrend(type='demean')
    st.merge(fill_value=0)
    return st

def get_OVPF_mseed_data(netwks,stas,cmps,start_date,end_date,input_dir,output_dir,overwrite=True):
    '''
    Get OVPF mseed data from start_date to end_date and convert it to sac
    Args:
        * stas: List of stations
        * cmps: List of components (e.g., [HHZ,HHN,HHE])
        * start_date: Start date
        * end_date:  End date
        * input_dir: Input directory including mseed files
        * output_dir: Output directory where SAC files will be written
    '''
    # Convert mseed to sac for stats
    for comp in cmps:
        for sta in stas:
            for netwk in netwks:
                kcomp = comp+'.D'
                #if sta == 'BOR' or sta == 'PBR':
                #    kcomp = 'EH'+comp[-1]+'.D'
                t = start_date
                while t <= end_date:
                    # Prepare directories
                    year = t.year
                    day  = t.julday
                    kyear = str(year) 
                    if not os.path.exists(os.path.join(output_dir,kyear)):
                        os.mkdir(os.path.join(output_dir,kyear))
                    odir = os.path.join(output_dir,kyear,'G%s.%03d'%(kyear,day))
                    if not os.path.exists(odir):
                        os.mkdir(odir)
                    # Get the mseed file
                    mseed_name = '%s.%s.00.%s.%d.%03d'%(netwk,sta,kcomp,year,day)   # Digital station
                    if sta == 'MAID':
                        sac_name = '%s.%s.00.%s.%d.%03d.SAC'%(netwk,sta[:3],kcomp,year,day)
                        ofile = os.path.join(odir,sac_name)
                    else:
                        ofile = os.path.join(odir,mseed_name+'.SAC')

                    if (not overwrite) and os.path.exists(ofile):
                        t += 86400
                        continue
                    # Convert to sac
                    #try:
                    #print(input_dir)
                    #sys.stdout.flush()
                    ifile = os.path.join(input_dir,kyear,netwk,sta,kcomp, mseed_name) # Standard broadband station
                    if os.path.exists(ifile):
                        try:
                            st = get_st(ifile)
                            st.write(ofile, format='SAC')
                        except:
                            print('Cannot convert %s to SAC'%(ifile))
                    else:
                        ifileHH = ifile
                        kcomp = 'EH'+comp[-1]+'.D'
                        mseed_name = '%s.%s.00.%s.%d.%03d'%(netwk,sta,kcomp,year,day) # Analog station
                        ifile = os.path.join(input_dir,kyear,netwk,sta,kcomp, mseed_name) # Digital not broadband
                        ofile = os.path.join(odir,mseed_name+'.SAC')
                        if os.path.exists(ifile):
                            try:
                                st = get_st(ifile)
                                st.write(ofile, format='SAC')
                            except:
                                print('Cannot convert %s to SAC'%(ifile))
                        else:
                            ifileEH = ifile
                            mseed_name = '%s.%s.90.%s.%d.%03d'%(netwk,sta,kcomp,year,day) # Analog station
                            ifile = os.path.join(input_dir,kyear,netwk,sta,kcomp,mseed_name) # Analog station
                            ofile = os.path.join(odir,mseed_name+'.SAC')
                            if os.path.exists(ifile):
                                try:
                                    st = get_st(ifile)
                                    st.write(ofile, format='SAC')
                                except:
                                    print('Cannot convert %s to SAC'%(ifile)) 
                            else:
                                print('%s, %s or %s not found'%(ifileHH,ifileEH,ifile))
                        
                    #except:
                    #    print('cannot retrieve %s'%(ifile))

                    t += 86400

    # All done
    return


def get_OVPF_scp_data(netwk,stas,cmps,start_date,end_date,input_dir,output_dir,server='pitonmanuel.ipgp.fr',username='sysop',overwrite=True):
    '''
    Get OVPF data using scp from start_date to end_date
    Args:
        * stas: List of stations
        * cmps: List of components (e.g., [HHZ,HHN,HHE])
        * start_date: Start date
        * end_date:  End date
        * input_dir: Input directory on distant server
        * output_dir: Output directory on distant server
    '''
    # Prepare mseed directory
    mseed_dir = tempfile.mkdtemp(prefix='mseed.',dir=output_dir)
    # Get data from server using scp
    ssh = SSHClient()
    ssh.load_system_host_keys()
    ssh.set_missing_host_key_policy(AutoAddPolicy())
    ssh.connect(server,username=username)
    scp = SCPClient(ssh.get_transport())
    n_scp_try = 2
    for comp in cmps:
        for sta in stas:
            kcomp = comp+'.D'
            if sta == 'BOR' or sta == 'PBR':
                kcomp = 'EH'+comp[-1]+'.D'
            t = start_date
            while t <= end_date:
                # Prepare directories
                year = t.year
                day  = t.julday
                kyear = str(year) 
                if not os.path.exists(os.path.join(output_dir,kyear)):
                    os.mkdir(os.path.join(output_dir,kyear))
                odir = os.path.join(output_dir,kyear,'G%s.%03d'%(kyear,day))
                if not os.path.exists(odir):
                    os.mkdir(odir)
                # Get the mseed file
                mseed_name = '%s.%s.00.%s.%d.%03d'%(netwk,sta,kcomp,year,day)
                ifile = os.path.join(input_dir,kyear,netwk,sta,kcomp, mseed_name)
                if sta == 'MAID':
                    sac_name = '%s.%s.00.%s.%d.%03d.SAC'%(netwk,sta[:3],kcomp,year,day)
                    ofile = os.path.join(odir,sac_name)
                else:
                    ofile = os.path.join(odir,mseed_name+'.SAC')
                if (not overwrite) and os.path.exists(ofile):
                    t += 86400
                    continue
                for scp_try in range(n_scp_try):
                    try:
                        scp.get(ifile,mseed_dir)
                        # Convert to sac
                        st = get_st(os.path.join(mseed_dir,mseed_name))
                        st.write(ofile, format='SAC')
                        break
                    except:
                        scp = SCPClient(ssh.get_transport())
                        print('cannot retrieve %s on %s (try %d/%d)'%(ifile,server,scp_try+1,n_scp_try))
                        sys.stdout.flush()

                t += 86400

    scp.close()
    ssh.close()

    # Cleanup directories
    shutil.rmtree(mseed_dir)

    # All done
    return


def get_waveforms(config_file,overwrite=True):

    # Parse Config
    config = configparser.ConfigParser()
    config.read(config_file)

    # Get server
    mseed = {}
    host = config.get('mseed_server', 'host')
    user = config.get('mseed_server', 'user')
    inppath = config.get('mseed_server', 'inppath')
    outpath = config.get('mseed_server', 'outpath')

    # Get times
    t1 = UTCDateTime(config.get('mseed_request','start_date'))
    t2 = UTCDateTime(config.get('mseed_request','end_date'))

    # Get stations
    stats = [s.strip() for s in config.get('network','stations').split(',')]
    chans = [c.strip() for c in config.get('network','channels').split(',')]
    netwk = config.get('network','network')

    # Get continuous data from scp
    get_OVPF_scp_data(netwk,stats,chans,t1,t2,inppath,outpath,server=host,username=user,overwrite=overwrite)

    # All done
    return

if __name__=='__main__':
    get_waveforms(sys.argv[1])


