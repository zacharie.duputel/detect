
# Externals
import os,sys,shutil
import configparser
import numpy as np
from copy import deepcopy
from obspy.core import UTCDateTime
from subprocess import call
from glob import glob

# Internals
from .TemplateRequest import TemplateRequest, VarNone
from .GetWaveforms import get_OVPF_mseed_data


class Detect(object):
    '''
    A class that deals with detections
    '''

    def __init__(self,config_file):
        '''
        Init of the class
        Args:
            * config_file: Input configuration file
        '''

        # Internal 
        self.__loopflag = False

        # Create a config object
        assert os.path.exists(config_file), 'Cannot find %s'%(config_file)
        config = configparser.ConfigParser()
        config.read(config_file)
        
        # Output run directory
        self.run_path = config.get('io', 'outdir')
        if not os.path.exists(self.run_path):
            os.mkdir(self.run_path)
    
        # Path to binaries
        self.cmd_detection = config.get('detect','cmd_detection')
        self.cmd_det2evts  = config.get('detect','cmd_det2evts')
        self.cmd_det2dt    = config.get('relocation','cmd_det2dt')
        self.cmd_mk_event  = config.get('relocation','cmd_mk_event')
        self.cmd_hypodd    = config.get('relocation','cmd_hypodd')
        
        # detect
        self.mindetect = config.getint('detect','mindetect')
        self.corrcoeff = config.getint('detect','corrcoeff')
        self.nsec_cmatch = config.getfloat('detect','nsec_cmatch')
        self.filter_fmin = config.getfloat('detect','filter_fmin')
        self.filter_fmax = config.getfloat('detect','filter_fmax')
        self.station = [s.strip() for s in config.get('detect','stations').split(',')]
        
        # relocation
        self.s_delays  = config.get('relocation','s_delays').strip()
        self.relocation_corrcoeff = config.getfloat('relocation','corrcoeff')
        self.reference_events = VarNone(config.get('relocation','reference_events'))
        self.maxndetect = config.getint('relocation','maxndetect')
        self.config_hypodd = config.get('relocation','config_hypodd')
        
        # Template request (if necessary)
        self.template_files = []
        self.eventids = []

        # Get mseed
        #self.host = config.get('mseed_server', 'host')
        #self.user = config.get('mseed_server', 'user')
        self.i_continuous_path = config.get('mseed_request', 'inppath')
        self.o_continuous_path = config.get('mseed_request', 'outpath')

        # Get times
        self.start = VarNone(config.get('mseed_request','start_date'),date=True)
        self.current = self.start
        self.end   = VarNone(config.get('mseed_request','end_date'),date=True)

        # Get stations
        self.continuous_stats = [s.strip() for s in config.get('network','stations').split(',')]
        self.continuous_chans = [c.strip() for c in config.get('network','channels').split(',')]
        self.continuous_netwk = [n.strip() for n in config.get('network','network').split(',')]

        # All done
        return

    def set_dates(self,starttime,endtime):
        '''
        Set time window for detections
        Args:
            * starttime: start time
            * endtime: end time
        '''
        self.start = deepcopy(starttime)
        self.current = self.start
        self.end   = deepcopy(endtime)

        # All done
        return

    def get_templates(self,config_file,request_data=True,overwrite=True):
        '''
        Request templates and append it to list of templates
        Args:
            * config_file:  Configuration file
            * request_data: if True -> request templates through arclink
            * overwrite:    if True -> overwrite template files
        '''
        treq = TemplateRequest(config_file)
        if request_data:
            treq.get_templates(overwrite=overwrite)
            for event_file,eventid in zip(treq.event_files,treq.eventids):
                if eventid not in self.eventids:
                    self.eventids.append(eventid)
                    self.template_files.append(event_file)
        else:
            for output_dir in treq.output_dir:
                template_file = os.path.join(output_dir,'template_list')
                assert os.path.exists(template_file), '%s not found'%(template_file)
                fd=open(template_file,'rt')
                for l in fd:
                    eventid = l.strip().split('/')[-1]
                    if eventid not in self.eventids:
                        self.eventids.append(eventid)
                        self.template_files.append(l.strip())
                fd.close()

        # All done
        return
        
    def get_continuous_waveforms(self,overwrite=True):
        '''
        Request continuous data 
        Args:
            * overwrite: if True: overwrite continuous waveforms
        '''
        netwk = self.continuous_netwk
        stats = self.continuous_stats
        chans = self.continuous_chans
        inppath = self.i_continuous_path
        outpath = self.o_continuous_path
        if self.__loopflag:
            print('Get continous waveforms for %s'%(self.current.isoformat()[:10]))
            sys.stdout.flush()
            get_OVPF_mseed_data(netwk,stats,chans,self.current,self.current,inppath,outpath,
                                overwrite=overwrite)
        else:
            print('Get continous waveforms for %s to %s'%(self.start.isoformat()[:10],self.end.isoformat()[:10]))
            sys.stdout.flush()
            get_OVPF_mseed_data(netwk,stats,chans,self.start,self.end,inppath,outpath,
                                overwrite=overwrite)

        # All done
        return

    def write_template_list(self,template_list_filename):
        '''
        Write template file
        Args:
            template_list_filename: Template filename
        '''
        fd = open(template_list_filename,'wt')
        for template_file in self.template_files:
            fd.write('%s\n'%(template_file))
        fd.close()

        # All done
        return

    def set_current_date(self,datetime):
        '''
        Set self.current
        Args:
            datetime: Current date to be set (UTCDateTime object)
        '''
        self.current = datetime

    def write_parfile(self,par_filename,template_list_filename='template_list'):
        '''
        Write parameter file
        Args:
            * par_filename: Parameter filename
            * template_list_filename: Template list filename
        '''
        
        # Run directory name
        kdate = self.current.strftime('%Y%j')
        dirname = os.path.join(self.run_path,kdate)

        # Write par file
        fd = open(par_filename,'wt') 
        if 'DET2DT.par' in par_filename:
            fd.write('% DIRECTORY THAT CONTAINS THE DETECTION FILES\n')
            fd.write('%s\n'%(dirname))
            fd.write('% FILENAME THAT INDICATES THE TABLE OF THE S DELAY\n')
            fd.write('%s\n'%(self.s_delays))
            fd.write('% MINIMUM VALUE OF THE CROSS-CORRELATION TO KEEP THE DELAY (IN %)\n')
            fd.write('%d\n'%(self.relocation_corrcoeff))
        else:   
            fd.write('% YEAR TO PROCESS\n')
            fd.write('%d\n'%(self.current.year))
            fd.write('% JULIAN DAY TO PROCESS\n')
            fd.write('%d\n'%(self.current.julday))
            fd.write('% MINIMUM CORRELATION FOR KEEPING DETECTION\n')
            fd.write('%d\n'%(self.corrcoeff))
        fd.write('% MAXIMUM FREQUENCY OVER WHICH TO COMPUTE COHERENCY\n')
        fd.write('%.2f\n'%(self.filter_fmax))
        fd.write('% MINIMUM FREQUENCY OVER WHICH TO COMPUTE COHERENCY\n')
        fd.write('%.2f\n'%(self.filter_fmin))
        if 'DET2DT.par' in par_filename:
            fd.write('% DELAY FILENAME\ndt.cc\n')
            fd.write('% EVENT # IN THE LIST WE SHOULD START (1 ALL NEW EVENTS)\n')
            if self.reference_events is None:
                fd.write('1\n')
            else:
                assert os.path.exists(self.reference_events), '%s not found'%(self.reference_events)
                f = open(self.reference_events,'rt')
                nevent = 0
                for l in f:
                    fname = l.strip()
                    if len(fname) > 0:
                        nevent += 1
                f.close()
                nevent += 1
                fd.write('%d\n'%(nevent))
        else:
            fd.write('% DIRECTORY (CONTINUOUS FILES, DETECTION FILES)\n')
            fd.write('%s\n'%(self.o_continuous_path))
            fd.write('%s\n'%(self.run_path))
            fd.write('% TEMPLATE LIST\n')
            fd.write('%s\n'%(template_list_filename))
        if 'DETMU.par' in par_filename:
            fd.write('% MINIMUM NUMBER OF STATION FOR DETECTION\n')
            fd.write('%d\n'%(self.mindetect))
            fd.write('% NUMBER OF STATIONS USED FOR DETECTION (FOLLOWED BY STATION NAMES)\n')
            nstat = len(self.station)
            stats = self.station
        else:
            if 'DET2EVTS.par' in par_filename:
                fd.write('% NUMBER OF SECONDS FOR MATCHING SIMILAR DETECTIONS\n')
                fd.write('%.2f\n'%(self.nsec_cmatch))
            fd.write('% STATIONS LIST PRECEDED BY THE NUMBER OF STATIONS\n')
            stats = []
            for chan in self.continuous_chans:
                for stat in self.continuous_stats:
                    stats.append(stat[:3]+chan[-1])
            nstat = len(stats)
        fd.write('%d\n'%(nstat))
        for i in range(nstat):
            fd.write('%s\n'%(stats[i]))

        # All Done
        return
    
    def prepare_run(self,cleanup=True):
        '''
        Prepare a detect 
        Args:
            * rmdir: if rmdir is true, remove run directory
        '''
        kdate = self.current.strftime('%Y%j')
        dirname = os.path.join(self.run_path,kdate)

        # Create dirname
        if not os.path.exists(dirname):
            os.mkdir(dirname)
        elif cleanup:
            shutil.rmtree(dirname)
            os.mkdir(dirname)
            d_name = self.current.strftime('%Y_%j')
            o_basename = 'D_%s_%02.0f_%02.0f_%02d'%(d_name,self.filter_fmin,self.filter_fmax,self.corrcoeff)
            o_filename = os.path.join(self.run_path,o_basename)
            print(o_filename)
            sys.stdout.flush()
            if os.path.exists(o_filename):
                os.remove(o_filename)
        # Create template_list file
        template_list_filename = os.path.join(dirname,'template_list')
        self.write_template_list(template_list_filename)
        # Create DETMU.par file
        detmu_file = os.path.join(dirname,'DETMU.par')
        self.write_parfile(detmu_file,template_list_filename)
        # Create DET2EVENTS file
        det2events_file = os.path.join(dirname,'DET2EVTS.par')
        self.write_parfile(det2events_file,template_list_filename)
        # Create DET2DT file
        det2dt_file = os.path.join(dirname,'DET2DT.par')
        self.write_parfile(det2dt_file)

        # All done
        return
    
    def run(self,detect=True,write_hdrdat=True,compute_delays=True,reloc=True,remove_continuous=False,cleanup=False):
        '''
        Run event detections and relocations
        Args:
            * detect: if True, run detections
            * compute_delays: if True, compute inter-event delays
            * reloc: if True, run relocations
            * remove_continuous: if True, remove continuous waveforms for the current day
            * cleanup: if True, remove_continous + hdr/dat files in the current directory
        '''
        kdate = self.current.strftime('%Y%j')
        dirname = os.path.join(self.run_path,kdate)
        print('--\nNEW RUN FOR %s'%(kdate))
        print('Directory: %s'%(dirname))
        sys.stdout.flush()
        
        # cd to run directory
        os.chdir(dirname)
        
        # Deal with log/hdr/dat files
        if detect and reloc:
            print('EVENT DETECTION')
            sys.stdout.flush()
            if os.path.exists('LOG'):
                shutil.rmtree('LOG')
            if detect:
                # Remove old hdr files 
                hdrfiles = glob('*.hdr')
                for hdrfile in hdrfiles:
                    datfile = hdrfile.split('.')[0]+'.dat'
                    os.remove(hdrfile)
                    os.remove(datfile)

        if not os.path.exists('LOG'):
            os.mkdir('LOG')
    
        # Detect new events
        if detect:
            # DETECT_EVT
            oo = open('LOG/_DETECT_EVT.LOG','wt')
            print('   '+self.cmd_detection)
            sys.stdout.flush()
            call(self.cmd_detection, shell=True, stdin=sys.stdin, stdout=oo, stderr=oo)
            oo.close()

        if write_hdrdat:
            # DET2EVTS
            oo = open('LOG/_DET2EVTS.LOG','wt')
            print('   '+self.cmd_det2evts)
            sys.stdout.flush()
            call(self.cmd_det2evts, shell=True, stdin=sys.stdin, stdout=oo, stderr=oo)
            oo.close()
        
        # Relocate events
        if reloc:
            print('EVENT RELOCATION')
            
            # Create events.txt
            print('   Creating Detection list')
            sys.stdout.flush()
            o_f1 = open(os.path.join(dirname,'detections.txt'),'wt')
            o_f2 = open(os.path.join(dirname,'events.txt'),'wt')

            # Read reference catalog
            i_f = open(self.reference_events,'rt')
            for l in i_f:
                o_f2.write(l.strip()+'\n')
            i_f.close()

            # Read detections for current day
            hdr_lst = []
            otimes  = []
            detections = glob(os.path.join(dirname,'*.hdr'))
            for detection in detections:
                hdr_lst.append(detection)
                datet = os.path.basename(detection)[:14]
                otime = self.current.strptime(datet,'%Y%m%d%H%M%S')
                otimes.append(otime-self.current)
            sortd = np.argsort(otimes)
            for i in sortd:
                o_f1.write(hdr_lst[i]+'\n')
            o_f1.close()
            
            
            # Read past N detections
            N = 0
            prev_day = self.current - 86400
            while 1:
                prev_kdate = prev_day.strftime('%Y%j')
                prev_dirname = os.path.join(self.run_path,prev_kdate)
                if not os.path.exists(prev_dirname) or N >= self.maxndetect:
                    break
                i_f = open(os.path.join(prev_dirname,'detections.txt'),'rt')
                for l in i_f:
                    hdrfile = l.strip()
                    if not os.path.exists(hdrfile):
                        print('WARNING: issue detected: cannot find %s'%(hdrfile))
                        continue
                    hdr_L = open(hdrfile,'rt').readlines()
                    if len(hdr_L) <= 3:
                        print('WARNING: issue detected for %s'%hdrfile)
                        continue
                    Nsta = int(hdr_L[2])
                    if len(hdr_L)-3 != Nsta:
                        print('WARNING: issue detected for %s'%hdrfile)
                        continue
                    hdr_lst.append(hdrfile)
                    datet = os.path.basename(hdrfile)[:14]
                    otime = self.current.strptime(datet,'%Y%m%d%H%M%S')
                    otimes.append(otime-self.current)
                    N += 1
                    if N >= self.maxndetect:
                        break 
                i_f.close()
                prev_day = prev_day - 86400
            
            sortd = np.argsort(otimes)
            # Append detections to events.txt file
            for i in sortd:
                o_f2.write(hdr_lst[i]+'\n')
            o_f2.close()

            # DET2DT
            if compute_delays:
                oo = open('LOG/_DET2DT.LOG','wt')
                print('   '+self.cmd_det2dt)
                call(self.cmd_det2dt, shell=True, stdin=sys.stdin, stdout=oo, stderr=oo)
                oo.close()

            # Prepare HypoDD
            shutil.copy(self.config_hypodd,dirname)
            print('   '+self.cmd_mk_event)
            oo = open('LOG/_make_event.LOG','wt')
            call(self.cmd_mk_event,shell=True,stdin=sys.stdin,stdout=oo,stderr=oo)
            oo.close()

            # HypoDD
            hypoinp = os.path.basename(self.config_hypodd)
            cmd_hypo = '%s %s'%(self.cmd_hypodd,hypoinp)
            print('   '+cmd_hypo)
            oo = open('LOG/_hypodd.LOG','wt')
            call(cmd_hypo,shell=True,stdin=sys.stdin,stdout=oo,stderr=oo)
            oo.close()
            print('--')
        
        if cleanup or remove_continuous:
            # Remove continuous detections
            kyear = str(self.current.year)
            day   = self.current.julday
            wav_dir = os.path.join(self.o_continuous_path,kyear,'G%s.%03d'%(kyear,day))
            shutil.rmtree(wav_dir)
            if cleanup: # Remove hdr and dat files
                hdrfiles = glob('*.hdr')
                for hdrfile in hdrfiles:
                    datfile = hdrfile.split('.')[0]+'.dat'
                    os.remove(hdrfile)
                    os.remove(datfile)

        # All done
        return
        

    def __iter__(self):
        return self

    def next(self):
        '''
        Iterator
        '''
        self.__loopflag = True
        if self.current > self.end:
            self.__loopflag = False
            self.current = self.start
            raise StopIteration
        else:
            dday = self.copy()    # Current day
            self.current += 86400 # Move to next day
            return dday

    def copy(self):
        return deepcopy(self)


