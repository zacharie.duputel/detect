
import os,sys
from copy import deepcopy
from obspy.core import UTCDateTime, Stream, read
from obspy.clients.arclink import Client
import configparser

def VarNone(inp_par,date=False,real=False):
    '''
    Parsing string to date or float
    Args:
        * date: if True, return date
        * real: if True, return float
    '''
    if inp_par == 'None':
        return None
    if date:
        return UTCDateTime(inp_par)
    if real:
        return float(inp_par)
    else:
        return inp_par


def get_OVPF_arclink_data(netwks,stats,t1,t2,host='195.83.188.22',port='18001',user='sysop', institution='OVPF'):
    '''
    Arclink request
    Args:
        * stats: list of stations
        * t1,t2: time-window
        * host: server host
        * port: server port
        * user: request user
        * institution: request institution name
    '''
    # Get waveforms using arclink
    client = Client(host=host, port=port, user=user, institution=institution)
    st = Stream()
    for kcmp in ['*Z','*N','*E']:
        for netwk in netwks:
            if netwk == 'ZF':
                try:
                    st1 = client.get_waveforms(netwk, '????', '??', kcmp, t1, t2)
                except:
                    continue
            else:
                st1 = client.get_waveforms(netwk, '???', '??', kcmp, t1, t2)
                try:
                    st2 = client.get_waveforms(netwk, '????', '00', kcmp, t1, t2)
                    for t in st2:
                        st1.append(t)
                except:
                    print('MAID not available for component '+kcmp[-1])
            # Select stations 
            for s in stats:
                sttmp = st1.select(station=s)
                for t in sttmp:
                    s_t = t.stats.starttime - t1 # beg. time - req. start time
                    e_t = t2 - t.stats.endtime   # end time - req. end time
                    dt  = t.stats.delta          # sampling step
                    if s_t<=dt and e_t<=dt:
                        st.append(t)
                    else:
                        print('Incomplete template: %s (rejected)'%(t.get_id()))
            sys.stdout.flush()

    # All done
    return st
 

class TemplateRequest(object):
    '''
    A class that deals with template requests
    '''
    
    def __init__(self,config_file=None):
        '''
        Init template request class
        '''
        self.set_server()
        self.set_output_dir()
        self.set_screening_dates()
        self.set_screening_lonlat()
        self.set_screening_stations()
        if config_file is not None:
            self.parse_config(config_file)

    def parse_config(self,config_file):
        '''
        Parse config file 
        '''

        # Create a config object
        assert os.path.exists(config_file), 'Cannot find %s'%(config_file)
        config = configparser.ConfigParser()
        config.read(config_file)

        # Templates from arclink
        self.host = config.get('templates_server', 'host')
        self.port = config.get('templates_server', 'port')
        self.user = config.get('templates_server', 'user')
        self.institution = config.get('templates_server', 'institution')

        # Template request parameters
        self.safety_delay = config.getfloat('templates_request','safety_delay')
        self.duration = config.getfloat('templates_request','duration')
        self.start = VarNone(config.get('templates_request', 'start_date'),date=True)
        self.end = VarNone(config.get('templates_request', 'end_date'),date=True)
        self.latmin = config.getfloat('templates_request', 'latmin')
        self.latmax = config.getfloat('templates_request', 'latmax')
        self.lonmin = config.getfloat('templates_request', 'lonmin')
        self.lonmax = config.getfloat('templates_request', 'lonmax')
        self.depmin = VarNone(config.get('templates_request', 'depmin'),real=True)
        self.depmax = VarNone(config.get('templates_request', 'depmax'),real=True)
        self.depsigma = VarNone(config.get('templates_request', 'depsigma'),real=True)

        # Template list
        self.event_files = None
        self.eventids = None

        # Station
        if config.has_option('network','stations'):
            self.stations = [s.strip() for s in config.get('network','stations').split(',')]
        else:
            self.stations = None
        self.network = [n.strip() for n in config.get('network','network').split(',')]
        
        # Bulletin
        self.bulletin = [b.strip() for b in config.get('templates_request', 'bulletin').split(',')]
        self.output_dir = [d.strip() for d in config.get('templates_request', 'outpath').split(',')]
        assert len(self.bulletin) == len(self.output_dir), '# of bulletins must be equal to # template outpaths in %s'%(config_file)

    def set_server(self,host='195.83.188.22',port='18001',user='sysop',institution='OVPF'):
        '''
        Set server attributes for arclink requests
        '''
        self.host = host
        self.port = port
        self.user = user
        self.institution = institution
    
    def set_output_dir(self,output_dir='./'):
        '''
        Set Output directory
        '''
        if type(output_dir)==list:
            self.output_dir = deepcopy(output_dir)
        else:
            self.output_dir = [output_dir]

    def set_screening_dates(self,starttime=None,endtime=None):
        '''
        Set screening starttime and endtime
        '''
        self.start = starttime
        self.end   = endtime

    def set_screening_lonlat(self,latmin=-90.,latmax=90.,lonmin=-180.,lonmax=180.,depmin=None,depmax=None):
        '''
        Set location screening
        '''
        self.latmin = latmin
        self.latmax = latmax
        self.lonmin = lonmin
        self.lonmax = lonmax
        self.depmin = depmin
        self.depmax = depmax

    def set_screening_stations(self,stations=None):
        '''
        Set station list
        '''
        self.stations = stations

    def get_templates(self,overwrite=False):
        '''
        Get templates
        '''
        # Print inputs
        print('Creating templates')
        print(self)
        sys.stdout.flush()

        # Check inputs
        if self.start is not None and self.end is not None:
            assert self.start < self.end
        assert self.latmin < self.latmax
        assert self.lonmin < self.lonmax
        assert len(self.bulletin) == len(self.output_dir), '# of bulletins must be equal to # template outpaths'

        # Loop on template catalogs
        self.event_files = []
        self.eventids    = []
        for bull,o_dir in zip(self.bulletin,self.output_dir):
            # Prepare directories
            if not os.path.exists(o_dir):
                os.mkdir(o_dir)
            # Read bulletin
            fs = open(bull,'r')
            ft = open(os.path.join(o_dir,'template_list'),'wt')
            flag = False
            while 1:
                txt = fs.readline()
                if "Public ID" in txt:
                    # Event id
                    eventid = txt.strip().split()[2]
                    flag = True
                elif "Date" in txt:
                    # Origin time
                    sdate  = txt.strip().split()[1]
                    txt   = fs.readline()
                    stime = txt.strip().split()[1]
                    txt   = fs.readline()
                    otime = UTCDateTime('%sT%sZ'%(sdate,stime))
                    # Hypocenter
                    lat   = float(txt.strip().split()[1])
                    txt   = fs.readline()
                    lon   = float(txt.strip().split()[1])
                    txt   = fs.readline()
                    depitems = txt.strip().split()
                    dep   = float(depitems[1])
                    try: 
                        depsigma = float(depitems[4])
                    except:
                        flag = False
                        continue
                    # Screening by location
                    if lat<self.latmin or lat>self.latmax:
                        flag = False
                        continue
                    if lon<self.lonmin or lon>self.lonmax:
                        flag = False
                        continue
                    if self.depmin is not None:
                        if dep<self.depmin:
                            flag = False
                            continue
                    if self.depmax is not None:
                        if dep>self.depmax:
                            flag = False
                            continue
                    if self.depsigma is not None:
                        if depsigma > self.depsigma:
                            flag = False
                            continue
                    # Screening by origin time
                    if self.start is not None:
                        if otime < self.start:
                            flag = False
                            continue
                    if self.end is not None:
                        if otime > self.end:
                            flag = False
                            continue
                elif 'preferred' in txt:    
                    # Magnitude
                    items = txt.strip().split()
                    if items[0] == 'Md':
                        Md = txt.strip().split()[1]
                    else:
                        Md = 'NaN'
                elif flag and "Phase arrivals v2 "in txt:
                    # Phase arrivals
                    txt = txt.split()
                    nb = int(txt[0])
                    fs.readline()
                    stas = []
                    Pt  = {}
                    Ptt = {}
                    for k in range(nb):
                        txt_phase= fs.readline().strip().split()
                        sta = txt_phase[0]
                        pha = txt_phase[4]
                        if pha == "P":
                            if self.stations is not None:
                                if sta not in self.stations:
                                    continue
                            stas.append(sta)
                            if txt_phase[5] == "C" or txt_phase[5] == "D" :
                                tim = txt_phase[6]
                            else:
                                tim = txt_phase[5]
                            # P-time
                            Pt[sta] = UTCDateTime('%sT%s'%(sdate,tim))
                            # P travel-time
                            Ptt[sta] = Pt[sta] - otime
                        else:
                            continue
                    # Check if hdr and dat file exist
                    filename = os.path.join(o_dir,eventid)
                    if os.path.exists(filename+'.hdr') and os.path.exists(filename+'.dat'):
                        if not overwrite:
                            print(eventid+' already downloaded')
                            self.event_files.append(filename)
                            self.eventids.append(eventid)
                            ft.write(filename+'\n')
                            flag = False
                            continue
                        else:
                            print(eventid+' overwrite')
                    else:
                        print(eventid)
                    sys.stdout.flush()
                    # Request data 
                    t1 = otime - self.safety_delay
                    t2 = otime + (max(Ptt.values()) + self.duration) + self.safety_delay
                    st = get_OVPF_arclink_data(self.network,stas,t1,t2)
                    sotime = otime.strftime('%Y%m%d%H%M%S.%f')[:17]
                    # Time-window and write output file
                    filename = os.path.join(o_dir,eventid)
                    f1 = open(filename+'.hdr','wt')
                    f1.write('%17s %10.5f %12.5f %10.5f %s\n'%(sotime,lat,lon,dep,Md))
                    f1.write('%s\n'%(eventid))
                    f1.write(u'%d\n'%(len(st)))
                    f2 = open(filename+'.dat','w+b')
                    st.detrend(type='demean')
                    for t in st:
                        # Time-window
                        station = t.stats['station']
                        t1 = Pt[station]-self.safety_delay
                        t.trim(starttime=t1,endtime=None)
                        dt = t.stats.starttime - otime 
                        # Write output
                        s = station[:3]
                        c = t.stats['channel'][-1]
                        f1.write('%-s%1s %6.3f %6.3f\n'%(s,c,dt,Ptt[station]))
                        # Write data
                        npts = int(self.duration/t.stats.delta + 1)
                        t.data[:npts].astype('int32').tofile(f2)
                    f1.close()
                    f2.close()
                    ft.write(filename+'\n')
                    self.event_files.append(filename)
                    self.eventids.append(eventid)
                    
                if txt =='':
                   break 
            fs.close()
            ft.close()

    def __str__(self):
        '''
        Screening params
        '''
        str = ''
        if self.start is not None:
            str += '  start: %s\n'%(self.start.isoformat())
        if self.end is not None:
            str += '  end: %s\n'%(self.end.isoformat())
        if self.latmin > -90.:
            str += '  latmin: %.2f\n'%(self.latmin)
        if self.latmax < 90.:
            str += '  latmax: %.2f\n'%(self.latmax)
        if self.lonmin > -180.:
            str += '  lonmin: %.2f\n'%(self.lonmin)
        if self.lonmax < 180.:
            str += '  lonmax: %.2f\n'%(self.lonmax)
        if self.depmin is not None:
            str += '  depmin: %.2f\n'%(self.depmin)
        if self.depmax is not None:
            str += '  depmax: %.2f\n'%(self.depmax)
        if len(str)>0:
            str = 'Template screening params:\n'+str
        else:
            str = ''

        # All done
        return str
